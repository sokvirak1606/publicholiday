﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CountryPublicHoliday.Class.Holiday
{
    public class Countries
    {
        public string CountryCode { get; set; }
        public List<string> Regions { get; set; }
        public List<string> HolidayTypes { get; set; }
        public string FullName { get; set; }
        public HolidayDate FromDate { get; set; }
        public HolidayDate ToDate { get; set; }

        public Countries() 
        {
            this.Regions = new List<string>();
            this.HolidayTypes = new List<string>();
        }
    }
}
