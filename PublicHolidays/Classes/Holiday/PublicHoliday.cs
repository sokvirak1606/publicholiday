﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CountryPublicHoliday.Class.Holiday
{
    public class PublicHoliday
    {
        public string CountryCode { get; set; }
        public string CountryFullName { get; set; }
        public List<HolidayName> HolidayName { get; set; }

        public PublicHoliday() 
        {
            this.HolidayName = new List<HolidayName>();
        }
    }
}
