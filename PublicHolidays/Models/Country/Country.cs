﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CountryPublicHoliday.Models.Country
{
    public class Country
    {
        public int Id { get; set; }
        public string CountryCode { get; set; }
        public string FullName { get; set; }
        public DateTime FromDate { get; set; }
        public DateTime ToDate { get; set; }

    }
    public class PublicHolidayCountry : Country
    {
        public ICollection<Regions> Regions { get; set; }
        public ICollection<HolidayType> HolidayTypes { get; set; }
    }
}
