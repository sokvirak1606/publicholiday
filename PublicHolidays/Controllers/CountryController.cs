﻿using CountryPublicHoliday.Models;
using CountryPublicHoliday.Models.Country;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;

namespace PublicHolidays.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class CountryController : ControllerBase
    {

        private readonly AppDbContext _context;
        public CountryController(AppDbContext context)
        {
            this._context = context;
        }

        [HttpGet]
        [Route("/api/GetListCountries")]
        public object Get()
        {
            List<Country> countries = new List<Country>();
            try
            {                
                using (var context = _context)
                {
                    #region Prepare Return Data
                    bool IsTrue = false;
                    var GetData = context.Countries.ToList();
                    if (GetData.Count == 0)
                    {
                        IsTrue = FunctionInsertData();
                        if (IsTrue)
                        {
                            GetData = context.Countries.ToList();
                        }
                        else
                        {
                            return new object();
                        }
                    }
                    foreach (var item in GetData)
                    {
                        countries.Add(new Country()
                        {
                            Id = item.Id,
                            CountryCode = item.CountryCode,
                            FullName = item.FullName,
                            FromDate = item.FromDate,
                            ToDate = item.ToDate
                        });
                    }
                    #endregion
                }
                return JsonConvert.SerializeObject(countries);
            }
            catch (Exception ex)
            {
                throw ex.GetBaseException();
            }            
        }

        [NonAction]
        public bool FunctionInsertData()
        {
            try
            {
                #region Insert data to database
                List<PublicHolidayCountry> lstCounties = new List<PublicHolidayCountry>();
                DataClassificationController dataClassification = new DataClassificationController();
                lstCounties = dataClassification.GetCounties();
                _context.AddRange(lstCounties);
                _context.SaveChanges();
                return true;
                #endregion
            }
            catch (Exception ex)
            {
                return false;
            }

        }
    }
}
