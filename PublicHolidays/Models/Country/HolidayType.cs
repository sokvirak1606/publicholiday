﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace CountryPublicHoliday.Models.Country
{
    public class HolidayType
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        public string  Type { get; set; }
        public PublicHolidayCountry publicHolidayCountries { get; set; }
    }
}
