﻿using CountryPublicHoliday.Class.Holiday;
using CountryPublicHoliday.Models.Country;
using System;
using System.Collections.Generic;

namespace PublicHolidays.Classes.PrepareDatas
{
    public class PrepareDataInsert
    {
        public List<PublicHolidayCountry> DataInsert(List<Countries> countries) 
        {
            try
            {
                #region Normalized data
                List<PublicHolidayCountry> countries_ = new List<PublicHolidayCountry>();
                foreach (var item in countries)
                {
                    #region Holiday Types
                    var lstTypes_ = new List<HolidayType>();
                    foreach (var items in item.HolidayTypes)
                    {
                        lstTypes_.Add(new HolidayType()
                        {
                            Type = items
                        });
                    }
                    #endregion

                    #region Regions
                    var lstRegions_ = new List<Regions>();
                    foreach (var items_ in item.Regions)
                    {
                        lstRegions_.Add(new Regions()
                        {
                            Region = items_
                        });
                    }
                    #endregion

                    #region Country
                    countries_.Add(new PublicHolidayCountry()
                    {
                        CountryCode = item.CountryCode,
                        FromDate = DateTime.Parse(item.FromDate.Month + "/" + item.FromDate.Day + "/" + item.FromDate.Year),
                        ToDate = DateTime.Parse(item.ToDate.Month + "/" + item.ToDate.Day + "/" + (item.ToDate.Year >= 9999 ? 9999 : item.ToDate.Year)),
                        FullName = item.FullName,
                        HolidayTypes = lstTypes_,
                        Regions = lstRegions_
                    });
                    #endregion
                }
                return countries_; 
                #endregion
            }
            catch (Exception)
            {
                throw;
            }    
        }
    }
}
