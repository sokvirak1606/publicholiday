﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CountryPublicHoliday.Class.Holiday
{    
    public class Holiday
    {
        public HolidayDateOfWeek Date { get; set; }
        public HolidayDateOfWeek ObservedOn { get; set; }
        public List<HolidayName> Name { get; set; }
        public string HolidayType { get; set; }
        public Holiday()
        {
            this.Name = new List<HolidayName>();
        }
    }

    
}
