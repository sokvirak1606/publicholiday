﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CountryPublicHoliday.Class.Holiday
{
    public class HolidayDate
    {
        public int Day { get; set; }
        public int Month { get; set; }
        public int Year { get; set; }
    }

    public class HolidayDateOfWeek : HolidayDate 
    {
        public int DayOfWeek { get; set; }
    }
}
