﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Net.Http;
using System.Net.Http.Headers;

namespace CountryPublicHoliday.Services
{
    public class ClientServiceController : ControllerBase
    {
        public string GetClientServices(string url)
        {
            using (var client = new HttpClient())
            {
                try
                {
                    var BaseUrl = url;
                    if (BaseUrl == null || BaseUrl == null) throw new Exception("End Point Error!");

                    client.BaseAddress = new Uri("https://kayaposoft.com/enrico/json/v2.0");
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                    var responseTask = client.GetAsync(BaseUrl);

                    responseTask.Wait();
                    var result = responseTask.Result;
                    if (result.IsSuccessStatusCode)
                    {
                        var readTask = result.Content.ReadAsStringAsync().Result;
                        var lstData = readTask;
                        return lstData;
                    }
                    else {
                        return null;
                    }
                }
                catch (Exception)
                {
                    return null;
                }
            }
        }

    }
}
