﻿using CountryPublicHoliday.Class.Holiday;
using CountryPublicHoliday.Models.Country;
using CountryPublicHoliday.Services;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using PublicHolidays.Classes.PrepareDatas;
using System;
using System.Collections.Generic;

namespace PublicHolidays.Controllers
{
    public class DataClassificationController : ControllerBase
    {
        #region Cosume End Point Country
        public List<PublicHolidayCountry> GetCounties()
        {
            try
            {
                #region Call HttpClient
                ClientServiceController clsClientService = new ClientServiceController(); 
                #endregion

                PrepareDataInsert prepareData = new PrepareDataInsert();
                List<Countries> lstCountry = new List<Countries>();
                List<PublicHolidayCountry> countries = new List<PublicHolidayCountry>();
                var getData = clsClientService.GetClientServices("?action=getSupportedCountries");
                if (getData != null)
                {
                    lstCountry = JsonConvert.DeserializeObject<List<Countries>>(getData);
                    countries = prepareData.DataInsert(lstCountry);
                }
                else
                {
                    throw new Exception("Convert End Point Error!");
                }

                return countries;
            }
            catch (Exception ex)
            {
                throw ex.GetBaseException();
            }
        }
        #endregion
    }
}
