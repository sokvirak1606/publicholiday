﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CountryPublicHoliday.Class.Holiday
{
    public class HolidayName
    {
        public string Lang { get; set; }
        public string Text { get; set; }

    }
}
