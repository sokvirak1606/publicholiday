﻿using CountryPublicHoliday.Models.Country;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace CountryPublicHoliday.Models
{
    public class AppDbContext: DbContext
    {
        public AppDbContext(DbContextOptions<AppDbContext> options)
            : base(options)
        { 
        }
        public DbSet<PublicHolidayCountry> Countries { get; set; }
        public DbSet<Regions> Regions { get; set; }
        public DbSet<HolidayType> Types { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<PublicHolidayCountry>()
            .HasMany(p => p.Regions)
            .WithOne(b => b.publicHolidayCountries);

            modelBuilder.Entity<PublicHolidayCountry>()
            .HasMany(p => p.HolidayTypes)
            .WithOne(b => b.publicHolidayCountries);
        }
    }





}
